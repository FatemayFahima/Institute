<?php
include_once "vendor/autoload.php";

use Pondit\Institute\Student;
use Pondit\Institute\Subject;
use Pondit\Institute\Teacher;
use Pondit\Institute\Group;
use Pondit\Institute\Mark;

$student1=new Student();
var_dump($student1);

$subject1=new Subject();
var_dump($subject1);

$teacher1=new Teacher();
var_dump($teacher1);

$group1=new Group();
var_dump($group1);

$mark1=new Mark();
var_dump($mark1);